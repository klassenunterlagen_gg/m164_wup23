# Bewertungsraster

## Dokumentation 10%

- Es gibt eine Grundstruktur mit einem `README.md`, in welchem alle Unterdokumente verlinkt sind.
- Unterdokumente für die Themen (Anforderungskatalog, Dokumentation der Tests, Dokumentation Modelle der Datenbank (konzeptionelles, logisches und physisches), Dokumentation Skripte) 

## Anforderungskatalog 10%

- Es gibt einen klaren Link woher die Daten kommen.
- Es gibt einen Link auf die Beschreibung der Daten (Allenfalls Excel-File das bei opendata heruntergeladen werden kann)
- Es gibt eine Liste mit Beschreibung welche Daten ausgewertet werden, nach welchen Kriterien

## Testbeschreibung 10%

- Es gibt eine Beschreibung welche Aggregationen verglichen werden
- Es gibt ein SQL-Skript select-Statements enthält welche den Vergleich ermöglichen.

## Automatisierung des Prozesses 30%

- Es gibt ein Import-Skript, welches die Datenbank dropped, die import Tabelle neu erstellt, und dann die Daten aus dem “csv”-File einliest.
- Es gibt ein Skript, welches die Entitäten-Tabellen erstellt, die es braucht, um die Daten zu normalisieren.
- Es gibt ein Skript, welches die Daten aus der Import-Tabelle in die Normalisierten Tabellen abfüllt.
- Es gibt ein Test-Skript, welches mittels Aggregationen Tests durchführt ob, die Daten in der Import-Tabelle den Daten in der normalisierten Form entsprechen. (siehe Testbeschreibung)

## Auswertung 20%
- Es gibt ein Skript, welches alle nötigen Auswertungen erzeugt.
- Die Auswertungen werden mit einem Skript ausgegeben.
- Es gibt ein Skript, das zusätzliche Tabellen anlegt und füllt, um Codes in der Import-Tabelle zu beschreiben. (z.B. in der Import Tabelle werden für die Sprach-Gebiete (ch01,ch02,ch03,ch04) in der Schweiz, eine Tabelle regionencodes erzeugt, welche wie folgt aussieht:

    ```
    MariaDB [test]> select * from regionencodes;
    +--------------+-------------------------+
    | regionencode | regionenname            |
    +--------------+-------------------------+
    | ch01         | Deutsch Schweiz         |
    | ch02         | Franzoesische Schweiz   |
    | ch03         | Italienische Schweiz    |
    | ch04         | Raetoromanische Schweiz |
    +--------------+-------------------------+
    ```

## Geschwindigkeit der Auswertung 20%

- Es werden, falls nötig, Indizes erzeugt, um die Geschwindigkeit der Auswertungen zu beschleunigen.
- Es werden keine unnötigen joins in Auswertungen gemacht.
- Es wird immer auf möglich effiziente Filterkriterien geachtet (keine unnötigen `like`)

