# Anleitung Bulkimport mit docker

1.	Erstellen Sie in einer Datenbank die Tabelle "PeronenDB_ihrnahme".
2.	Legen Sie einen neuen Ordner in ihrem Computer an, z.B. «C:\bulkimport».
3.	Kopieren Sie das [CSV File](./x_res/personen.csv) in den obigen neu erstellten Ordner.
4.	Öffnen Sie nun ein das Commandline-Tool «cmd» und wechseln Sie zum Ordner «C:\bulkimport».
5.	Führen Sie diese Zeile aus:
    ```bash
    docker cp personen.csv mysql:/var/lib/mysql
    ```
    Das file wird vom Host (Computer) zum Container kopiert.

6.	Wechseln Sie zu MySql Workbench und öffnen Sie ein neues Query-Fenster. Nun können Sie das *LOAD DATA INFILE* Statement ausführen. Achten Sie auf den Filenamen.
 
    ```sql
    LOAD DATA INFILE './personen.csv' 
    INTO TABLE usw....
    ```
