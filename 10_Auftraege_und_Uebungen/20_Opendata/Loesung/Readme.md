# Musterlösung Auftrag Odata

1. CSV analysieren und nach Redundanzen suchen.
2. Werte wiederholen sich (redundant) und weisen auf NF2 hin.
3. Gem. 2 Normalfall Regel ergeben sich somit insgesamt 4 Tabellen
   ![](../../../x_gitressourcen/Normalisieren%20Steuern.png)

4. Folgende Tabellen enstehen:
   - Jahr
   - Quartier
   - Steuertarif
   - Steuern
5. ERD Modell zeichnen, siehe [ERD Modell](Steuern.mwb)
   
      ![](../../../x_gitressourcen/Steuern%20ERD.png)
6. Das ursprüngliche CSV File aufteilen in vier files (Redundanzen entfernen, z.B. in Excel mit *Duplikate entfernen*)
   - Pro CSV File gem. DDL Definition anpassen, sodass Bulkimport ohne Fehler durchläuft.
   - Achtung: Die FK's müssen manuell eingefügt werden!