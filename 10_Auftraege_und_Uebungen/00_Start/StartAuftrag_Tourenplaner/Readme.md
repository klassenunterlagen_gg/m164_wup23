# 01 StartAuftrag Tourenplaner
Lesen Sie zuerst aus dem Skript [Vorgehensweise DB Entwicklung.pdf](../../../02_Theorie_Unterlagen/Vorgehensweise_DB_Entwicklung.pdf) das Kapitel 4 aufmerksam durch. Diese Themen haben Sie bereits im Modul 162 kennengelernt und angewendet. Vertiefen Sie Ihr Wissen, indem sie das Gelesene mit dem nachfolgenden Auftrag in die Praxis umsetzen:


Ein Busunternehmen beschäftigt Disponenten und Fahrer. Die Disponenten planen und organisieren die Fahrten. Eine Fahrt hat mehrere Stationen, wobei Orte nur einmal angefahren werden dürfen. An jeder Station kann der Fahrer wechseln. Pro Station kann eine Ankunfts- und eine Abfahrtszeit erfasst werden, wobei für die Start-Station nur Abfahrtszeit erfasst wird und für die Ziel-Station nur die Ankunftszeit. Fahrer und Disponenten sind über Telefon erreichbar. Jeder Fahrt wird jeweils ein Fahrzeug zugeordnet, welches eine gewisse Anzahl Sitzplätze zur Verfügung stellt. 

![Skizze](./Tourenplaner.jpg)

Die Fahrten sind in einem Excel-Sheet registriert. Ein Beispiel wie dies bis jetzt mit Excel erfasst wurde finden sie im File [01_Auftrag Tourenplaner_Daten.xlsx](./01_Startauftrag_Tourenplaner_daten.xlsx).

## Aufgaben

1.	Analysieren Sie die obige Anforderung und zeichnen Sie ein konzeptionelles Datenmodell in draw.io
    -	Modell enthält nur Entitätstypen (Tabellen) und Beziehungen

2.	Setzen Sie ihre Entwicklung fort, indem Sie ein logisches Datenmodell im MySQL-Workbench zeichnen.
    -	Beschreiben Sie die notwendigen Tabellen und Beziehungen
    -	Bringen sie ihr Datenmodell in die 3. Normalform (Normalisierung von Datenbanken)

Zeit: 90 Min.
Form: 3er Team
