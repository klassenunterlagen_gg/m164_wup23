![TBZ Logo](../../../x_res/tbz_logo.png)
![m319 Picto](../../../x_res/m164_picto.jpg)

[TOC]

# ---- Recap m162 (PAT) ---

Zeitvorgabe: 15 Minuten <br>
Form: Individuell

## Wissensfragen

1. Was sind Constraints? <br>
   Zählen Sie diese auf und erklären Sie.
2. Zählen Sie die vier Beziehungstypen und deren Bedeutung auf. 
3. Was bedeutet referentielle Integrität in einer Datenbank?
4. Terminologie <br>
   Beschreiben das Bild mit den richtigen Fachbegriffen
   ![Terminologie](./Tabelle_labelled.png)