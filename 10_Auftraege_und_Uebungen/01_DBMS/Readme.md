# 02 Auftrag DBMS
Lesen Sie aus dem [Skript\_M164\_Themenübersicht](02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf) **das Kapitel 1** aufmerksam durch und lösen Sie dazu folgende Aufgaben: 

## Aufgaben

1.	Erstellen Sie ein **Mindmap** in ihrem Lernportfolio zu den **Kapiteln 1 bis 1.3**. Unklare Begriffe sollten Sie im Internet kurz nachschlagen oder die Lehrperson fragen. 

2.	**Kapitel 1.4** listet einige DB-Engine-Produkte auf. 
    -	Vergleiche Sie die Liste mit der aktuellen DB-Ranking: [DB-Engine Ranking](https://db-engines.com/en/ranking)
    -	Nehmen Sie die ersten 10 DB-Engines in ihr Mindmap auf

Zeit: 30 Min. <br>
Form: 2er Team
