![TBZ Logo](./x_res/tbz_logo.png)
![m319 Picto](./x_res/m164_picto.jpg)

[TOC]

# m164 - Datenbanken erstellen und Daten einfügen

1.Lehrjahr

## Modulbeschreibung:

Implementiert ein logisches, relationales Datenmodell in einem Datenbankmanagementsystem. Fügt Daten in die Datenbank ein, prüft die eingefügten Daten und korrigiert allfällige Fehler.

Einige Voraussetzung sind im Modul 162 zu finden (Normalisierung, Design).

[Modul-ID und Kompetenzmatrix](https://gitlab.com/modulentwicklungzh/cluster-data/m164/-/blob/master/1_Kompetenzmatrix/README.md)

[Modul-ID als PDF](./164_1_Datenbanken_erstellen_und_Daten_einfuegen.pdf)

### Mögliche LBVs

[LBV 164-1](https://gitlab.com/ict-modulformation-ch/module/m164/m164-lbv-1/-/blob/master/LBV_M164-1.md) 2-3 Kompetenznachweise (⅓ Theorie, ⅔ Praxis)

[LBV 164-3](https://gitlab.com/ict-modulformation-ch/module/m164/lbv-modul-164-3) 1 Prüfung & 1 Projekt für Fortgeschrittene

---

# Ablauf (Lektionenplan)


## Tag 01 - RECAP m162

- Vorstellung Klasse
- Vorstellung LP <br>
- Einführung Modul (Struktur Teams, GitLAB, [Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio))
- Recap m162
   - [Recap Fragen PAT](./10_Auftraege_und_Uebungen/00_Start/Recap_Fragen/Readme.md)
   - [Recap Fragen KEL](./10_Auftraege_und_Uebungen/00_Start/Recap_Fragen/Recap_KEL.md)
   - Auftrag [Tourenplaner](./10_Auftraege_und_Uebungen/00_Start/StartAuftrag_Tourenplaner/Readme.md) (Konzeptionelles & Logisches ERD, Normalisieren)
   
- [Installation DBMS](././01_Installation_SW) &#10142; XAMPP / docker / AWS


## Tag 02 - DDL

- Recap / Q&A Tag 01 <br>
- Ergänzung zu Tourenplaner: 
  - Generalisierung / Spezialisierung (Person mit der Rolle als Fahrer oder Disponent). Siehe **Kapitel 5** unter [Skript\_M164\_Themenübersicht](02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf)
  - Indentifying / Non-Identifying relationship, Siehe **Kapitel 8** unter [Skript\_M164\_Themenübersicht](02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf)
- Was ist ein Datenbank Management System? 
  - Beantworten Sie diese Frage mit folgendem [Auftrag](10_Auftraege_und_Uebungen/01_DBMS). 
- Einführung und Aufträge in [DDL](02_Theorie_Unterlagen/DDL/Readme.md):
  - Forward Engineering
  - Felddatentypen
  - CREATE SCHEMA
  - CREATE TABLE
  - DROP TABLE
- Auftrag für [Fortgeschrittene](10_Auftraege_und_Uebungen/10_DDL/04_storage_erklaeren_fuer_fortgeschrittene.md)

## Tag 03 - DML / DQL
- Recap / Q&A Tag 02
- [DDL](02_Theorie_Unterlagen/DDL/Readme.md) Ergänzung
  - ALTER TABLE
- Einführung zu **INSERT**: Im Plenum [Kurzform und Langform von INSERT-Statement](02_Theorie_Unterlagen/DML/05_insert-into.pdf) erklären und im Plenum üben.
  - Aufträge [DML](02_Theorie_Unterlagen/DML/Readme.md) (Data Manipulation Language)
- Einführung zu **SELECT**: Im Plenum [Select Befehl](02_Theorie_Unterlagen/DQL/06_select.pdf) erklären und im Plenum üben.
  - Aufträge [DQL SELECT](02_Theorie_Unterlagen/DQL/DQL_SEL.md) (Data Query Language)


## Tag 04 - LB1 / DQL

- Recap / Q&A Tag 03

> ! Tag 04 oder Tag 05: <br>
> **LB 1: Recap M162, DBMS, General/Spezial, Non/Identifying, DDL, DML, DQL (Inhalt Tag 1 - Tag 3)**  <br>
> ! 

- Auftrag [CREATE Beziehungen](02_Theorie_Unterlagen/DDL/CREATE_Bez.md)
- Erklärung im Plenum: [Einführung in die Mengenlehre](02_Theorie_Unterlagen/DQL/Theorie_Mengenlehre.md)
  - Auftrag [Mengenlehre](10_Auftraege_und_Uebungen/14_DQL/08_Auftrag_Mengenlehre.md)
- Einführung zu **SELECT JOIN**: Im Plenum [Select JOIN Befehl](02_Theorie_Unterlagen/DQL/07_select_JOINs.pdf) erklären und im Plenum üben.
  - Aufträge [DQL SELECT JOIN](02_Theorie_Unterlagen/DQL/DQL_SEL_JOIN.md) (Data Query Language)

## Tag 05 - DQL
- Recap / Q&A Tag 04
- Einführung im Plenum: [Datenintegrität](02_Theorie_Unterlagen/Datenintegritaet/Datenintegritaet_in_DB.pdf) und Auftrag [Datenkonsistenz, Datenintegrität, referentielle Integrität](02_Theorie_Unterlagen/Datenintegritaet/Readme.md)
- Einführung und Auftrag zu [SELECT ALIAS](02_Theorie_Unterlagen/DQL/DQL_SEL_ALIAS.md)
- Einführung und Auftrag zu [Aggregatsfunktionen](02_Theorie_Unterlagen/DQL/Aggregatsfunktionen.md)
- Einführung im Plenum [SELECT GROUP BY](02_Theorie_Unterlagen/DQL/select_group_by.pdf)
  - Auftrag [SELECT GROUP BY](10_Auftraege_und_Uebungen/14_DQL/20_select_group_by.md)  
- Einführung im Plenum [SELECT HAVING](02_Theorie_Unterlagen/DQL/select_having.pdf)
  - Auftrag [SELECT HAVING](10_Auftraege_und_Uebungen/14_DQL/22_select_having.md)
- Auftrag [SELECT GROUP BY HAVING SORT](10_Auftraege_und_Uebungen/14_DQL/14_select_groupby_sort.md)

## Tag 06 - DQL
- Recap / Q&A Tag 05
- Einführung Datensicherung
  - **Kapitel 6** lesen, [Skript\_M164\_Themenübersicht](02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf)
  - Auftrag [Datensicherung](10_Auftraege_und_Uebungen/18_Datensicherung/Readme.md)
- Einführung zu **Bulk Import**
  - Auftrag [Bulkimport](10_Auftraege_und_Uebungen/16_Bulkimport/Readme.md)
- Auftrag Fortgeschrittene: [HR Datenbank](10_Auftraege_und_Uebungen/99_Spezialauftraege_für_Fortgeschrittene/HR%20Datenbank/Readme.md)

 &#10142; Projektstart für Profis
  
## Tag 07 - DQL
- Recap / Q&A Tag 06
- Einführung [Select mit Subquery](02_Theorie_Unterlagen/DQL/DQL_SEL_SUBQUERY.md)
  - Auftrag [Subselect](10_Auftraege_und_Uebungen/14_DQL/24_select_subquery.md)
- Einfühung [Opendata in DB einbinden](02_Theorie_Unterlagen/OpenData/)
  - Auftrag [Opendata](10_Auftraege_und_Uebungen/20_Opendata/Readme.md) beginnen



## Tag 08 - DQL
- Recap / Q&A Tag 07
- Auftrag [Opendata](10_Auftraege_und_Uebungen/20_Opendata/Readme.md) fertigstellen
- Lösung [Opendata](10_Auftraege_und_Uebungen/20_Opendata/Loesung/)
- Vertiefung: [Migros Datenbank](10_Auftraege_und_Uebungen/99_Spezialauftraege_für_Fortgeschrittene/Migros%20Datenbank/Readme.md)

## Tag 09 & 10 - Diverses
- Alle Aufträge abschliessen
- Fragen LB2
- Auftrag für Fortgeschrittene: [DB Mondial](10_Auftraege_und_Uebungen99_Spezialauftraege_für_Fortgeschrittene/DB%20Mondial/Readme.md)

> ! Tag 09 oder Tag 10: <br>
> **LB 2: <br> THEORIE (⅓): Mengenlehre, Konsistenz und ref.Integrität, Aggregationsfunktionen, Bulk-Import & Datensicherung, Konzept 'Opendata einbinden in DB' <br> PRAXIS (⅔): DML, DQL (SELECT  ... HAVING..., SubSelect), Bulk BULK-Import, Dump <br> (Inhalt Tag 1 - Tag 7)**  <br>
> !


---

![moduluebersicht_m164](./x_res/m164_modulübersicht-Theorie.drawio.png)

---

![](./x_res/Buch.jpg)

# Referenz zu den Unterlagen, Aufträgen und Vorgaben

1. [Installation Tools (DBMS & Clients)](./01_Installation_SW)
2. [Theorie Unterlagen](./02_Theorie_Unterlagen)
2. [Aufträge und Übungen](./10_Auftraege_und_Uebungen)
3. [Projektarbeit (für Fortgeschrittene)](./20_Projektarbeit)