# DQL (Data Query Language)

[TOC]

## Aggregatsfunktionen

In MySQL gibt es Aggregatsfunktionen, die verwendet werden, um Daten in einer Tabelle zusammenzufassen oder zu berechnen. Diese Funktionen werden normalerweise in Verbindung mit dem GROUP BY-Statement verwendet, um Daten in Gruppen zu gruppieren und dann auf diesen Gruppen berechnen oder zusammenfassen zu können.

Hier sind einige der häufigsten Aggregatsfunktionen in MySQL:

### COUNT
Diese Funktion gibt die Anzahl der Zeilen in einer Tabelle oder Gruppe zurück. Sie kann auch verwendet werden, um die Anzahl von NULL-Werten in einer Spalte zu zählen.

Beispiel:

```sql
SELECT COUNT(*) FROM customers;
```
Das gibt die Anzahl aller Datensätze in der Tabelle "customers" zurück.

```sql
SELECT COUNT(salary) FROM customers;
```

Das gibt die Anzahl aller Zeilen der Spalte 'salary' **ohne NULL** in der Tabelle "customers" zurück.

### SUM
Diese Funktion berechnet die Summe der Werte in einer Spalte oder Gruppe.

Beispiel:

```sql
SELECT SUM(salary) FROM employees;
```
Das gibt die Summe der Werte in der Spalte "salary" der Tabelle "employees" zurück.

### AVG
Diese Funktion berechnet den Durchschnittswert der Werte in einer Spalte oder Gruppe.

Beispiel:

```sql
SELECT AVG(salary) FROM employees;
```
Das gibt den Durchschnittswert der Werte in der Spalte "salary" der Tabelle "employees" zurück.

### MIN
Diese Funktion gibt den kleinsten Wert in einer Spalte oder Gruppe zurück.

Beispiel:

```sql
SELECT MIN(salary) FROM employees;
```
Das gibt den kleinsten Wert in der Spalte "salary" der Tabelle "employees" zurück.

### MAX
Diese Funktion gibt den größten Wert in einer Spalte oder Gruppe zurück.

Beispiel:

```sql
SELECT MAX(salary) FROM employees;
```
Das gibt den größten Wert in der Spalte "salary" der Tabelle "employees" zurück.

Diese Aggregatsfunktionen können auch in Kombination verwendet werden, um komplexe Abfragen auszuführen und zusammengefasste Ergebnisse zu erhalten.

>Das Dokument [Aggregatsfunktionen](Aggregatfunktionen.pdf) liefert Ihnen detaillierte Erklärungen und praktische Beispiele sowie Übungen aus der [kundenDatenbank](../../10_Auftraege_und_Uebungen/90_Dumps/kundenDatenbank_dump.sql).

**Auftrag**: Setzen Sie den [Auftrag Aggregatsfunktionen](../../10_Auftraege_und_Uebungen/14_DQL/18_Aggregatsfunktionen.md) um.

---
Quellen:<br>
https://dev.mysql.com/doc/refman/8.0/en/aggregate-functions.html